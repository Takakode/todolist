<?php
require_once('../src/TaskManager.php');
require_once('../src/TaskItem.php');

$taskManager = TaskManager::getInstance();
$taskManager->downloadFromDatabase();

if(isset($_GET['id'])) {
    $task = $taskManager->getTaskItem($_GET["id"]);
    echo $task->toJson();
} elseif ( isset($_POST['title']) ) {
    $arrayTaskValues = array();
    $taskPropertiesNames = TaskItem::getPropertiesArray();
    foreach ($taskPropertiesNames as $value) {
        if ( isset($_POST[$value]) ) {
            $arrayTaskValues[TaskItem::convertNameVarToDB($value)] = htmlspecialchars($_POST[$value]);
        }
    }
    $task = TaskItem::constructWithArray($arrayTaskValues);
    $taskManager->commitTaskToDatabase($task);
    // temporaire faire la req avec vue.js maint
    header('Location: http://localhost:9999/public/index.php');
    exit();
} else {
    echo json_encode($taskManager->getTasks());
}
