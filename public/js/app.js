console.log("ok");
var server = "http://localhost:9999/public/"

Vue.component(
    'deleteTask',
    {
        props:['taskId'],
        template:'<button type="button" class="btn btn-danger">Delete Task</button>'
    }
)

Vue.component(
    'validateTask',
    {
        props:['taskId'],
        template:'<button type="button" class="btn btn-success">Validate task</button>'
    }
)

Vue.component(
    'taskShowDates',
    {
        props:['dateStart','dateEnd'],
        template:'<div> Deadline : {{ timeStampToDate(dateEnd) }}</div>',
        methods:{
            timeStampToDate: function(timestamp) {
                let date = new Date(timestamp * 1000);
                let jour = date.getDate();
                let month = date.getMonth();
                let year = date.getFullYear();
                return  jour+"/"+month+"/"+year;
            }
        }

    }
)

Vue.component(
    'todo-item',
    {
        props: ['todo'],
        template: '<li class="list-group-item">'
            + '<div class="row">'
                + '<h3>'
                + '{{ todo.title[0].toUpperCase() + todo.title.slice(1) }}'
                + '</h3>'
                + '<taskShowDates v-if="todo.dateStart" v-bind:dateStart="todo.dateStart" v-bind:dateEnd="todo.dateEnd"></taskShowDates>'
            + '</div>'
            + '<div class="row"> {{ todo.text }} </div>'
            + '<div class="row"><validateTask></validateTask><deleteTask></deleteTask></div> '
            + '</li>'
    }
    )

var vm = new Vue({
    el: '#app',
    data: {
        message:'Hello world!',
        taskList:null
    },
    mounted: function () {
        console.log("Mounting");
        this.getTasks();
        console.log("Mounted");
    },
    methods: {
        getTasks: function(){
            var self = this;
            let url = server+"task.php";
            fetch(url,{method:'GET'}).then(function(response){
                return response.json();
            }).then( function(data){
                self.taskList = data;
                console.log(data);
            }).catch(function(error){
            });
        }
    }
});