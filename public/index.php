<!DOCTYPE html>
<html lang="eng">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="static/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <script src="static/vue.js"></script>
        <!-- production version, optimized for size and speed -->
        <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    </head>
    <body>
    <header>
        <h1> Test Todo List</h1>
    </header>

    <div class="container" id="app">
            <div class="row">
                    <form action="task.php" method="post">
                        <div class="form-group">
                            <label for="title">Enter your task's title:</label><input class="form-control" type="text" name="title" id="title" required>
                        </div>
                        <div class="form-group">
                            <label for="text">Enter task's description:</label><input class="form-control" type="text" name="text" id="text">
                        </div>
                        <div class="form-group">
                            <label for="dateEnd">Select the limit date</label><input class="form-control" type="date" name="dateEnd" id="dateEnd">
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </form>
            </div>
        </br>
            <div class="row">
                <ul class="list-group">
                <todo-item
                        v-for="item in taskList"
                        v-bind:todo="item"
                        v-bind:key="item.id"
                ></todo-item>
                </ul>
            </div>
    </div>

    <script src="static/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="static/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="static/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/app.js"></script>
    </body>
</html>