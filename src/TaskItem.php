<?php


class TaskItem implements \JsonSerializable {
    // TODO think about how to manage variable not set in a more beautiful way
    private ?int $id;
    private ?int $idGroup;
    private string $title ="";
    private string $text = "";
    private ?int $dateStart = null;
    private ?int $dateEnd = null;
    /**
     * @var bool
     */
    private bool $validate = false;

    private function __construct()
    {
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
    /**
     * @param array $argumentsList
     * @return TaskItem
     * Permit to construct a task with an Array
     */
    public static function constructWithArray(array $argumentsList) : TaskItem
    {
        $objectResult = new TaskItem();

        foreach($argumentsList as $key => $value) {
            if(!empty($value)){ // == 0 should be true when value=0 and value= "0"
                switch ($key) {
                    case "id":
                        $objectResult->setId($value);
                        break;
                    case "id_group":

                        $objectResult->setIdGroup($value);
                        break;
                    case "validate":
                        if($value == 0) {
                            $objectResult->setValidate(false);
                        } else {
                            $objectResult->setValidate(true);
                        }
                        break;
                    case "date_start":
                        //echo strtotime($value);
                        $objectResult->setDateStart(strtotime($value));
                        break;
                    case "date_limit":
                        $objectResult->setDateEnd(strtotime($value));
                        break;
                    case "title":
                        $objectResult->setTitle($value); // TODO :  escape text for protecting from malicious code
                        break;
                    case "text":
                        $objectResult->setText($value); // TODO :  escape text for protecting from malicious code
                        break;

                }
            }

        }

        return $objectResult;
    }

    /** A method that return the object to the form of an associative array
     * @return array
     */
    public function toArray(): array
    {
        return [
            "id"=>$this->getId(),
            "id_group" => $this->getIdGroup(),
            "validate" => $this->isValidate(),
            "date_start" => $this->getDateStart(),
            "date_limit" => $this->getDateEnd(),
            "title"=> $this->getTitle(),
            "text"=> $this->getText()
        ];
    }

    /** A method that return the object to a json form
     * @return string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdGroup(): ?int
    {
        return $this->idGroup;
    }

    /**
     * @param int $idGroup
     */
    public function setIdGroup(int $idGroup): void
    {
        $this->idGroup = $idGroup;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return int|null
     */
    public function getDateStart(): ?int
    {
        return $this->dateStart;
    }

    /**
     * @param int $dateStart
     */
    public function setDateStart(int $dateStart): void
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return int|null
     */
    public function getDateEnd(): ?int
    {
        return $this->dateEnd;
    }

    /**
     * @param int $dateEnd
     */
    public function setDateEnd(int $dateEnd): void
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return bool
     */
    public function isValidate(): bool
    {
        return $this->validate;
    }

    /**
     * @param bool $validate
     */
    public function setValidate(bool $validate): void
    {
        $this->validate = $validate;
    }

    /** in the database it saved as 1 or 0 for boolean
     * @return int
     */
    public function getValidateForDB() : int
    {
        return $this->isValidate() ? 1 : 0;
    }

    public static function convertNameVarToDB(string $name) : string
    {
        $res="";
        switch ($name) {
            case "idGroup":
                $res = "id_group";
                break;
            case "validate":
                $res = "validate";
                break;
            case "text":
                $res = "text";
                break;
            case "title":
                $res = "title";
                break;
            case "dateStart":
                $res = "date_start";
                break;
            case "dateEnd":
                $res = "date_limit";
                break;
        }
        return $res;
    }

    public static function getPropertiesArray() : array
    {
        $task = new TaskItem();
        return array_keys(get_object_vars($task));
    }
}