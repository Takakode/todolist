<?php

class MysqlConnector {
    private static string $server ='localhost:8889';
    private static string $username= 'root';
    private static string $password ='root';
    private static string $DbName   = 'project_todo';

    private function __construct(){
    }

    public static function getCon() : PDO
    {
        try {
            $conn = new PDO("mysql:host=".self::$server.";dbname=".self::$DbName, self::$username, self::$password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $conn;
        } catch(PDOException $e) {
            echo "error connection database";
            die();
        }
    }
}