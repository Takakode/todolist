<?php

require_once("MysqlConnector.php");
require_once("TaskItem.php");

// TODO think about how to dissociate database operation from the domain work


/** Singleton that manage the TaskItem objects of the task list
 * Class TaskManager
 */
class TaskManager {
    private array $list = [];
    private static TaskManager $instance;

    private function __construct(){
    }

    /**
     * @return TaskManager
     */
    public static function getInstance(): TaskManager
    {
        if( !isset(self::$instance)) {
            self::$instance = new TaskManager();
        }
        return self::$instance;
    }

    public function downloadFromDatabase() : void
    {
        $mc = MysqlConnector::getCon();
        if( is_null($mc)){
            return; // Note: good practice ?
        }

        $sqlQuery = 'SELECT * FROM task';
        $result = $mc->query($sqlQuery)->fetchAll();
        //var_dump($result);

        foreach ($result as $item) {
            //var_dump($item);
            $itemTask = TaskItem::constructWithArray($item);
            if($itemTask){
                $this->addTaskItem($itemTask);
            }
        }
    }

    /** Check if there is an id, if the id exist then the task already exist in the database if not we insert
     * @param MysqlConnector $mc
     * @param TaskItem $task
     */
    public function commitTaskToDatabase(TaskItem $task) : void
    {
        $mc = MysqlConnector::getCon();
        if(is_null($task->getId())) {
            $this->insertTaskToDatabase($mc,$task);
        } else {
            // we first check if the task don't already exist should not be the case but let be safe
            $sqlQuery = "SELECT FROM task WHERE id='".$task->getId()."'";
            $result = $mc->query($sqlQuery)->fetchAll(); // should get only one since we look with an unique id
            // must be care full about the update and insert
            // if there is already a result we do an update, if not insert
            if(sizeof($result)>0) {
                // update part
                $this->updateTaskToDatabase($mc,$task);
            } else {
                // insert part
                $this->insertTaskToDatabase($mc,$task);
            }
        }

    }

    // TODO test
    public function updateTaskToDatabase(PDO $mc, TaskItem $task) {
        $sqlQuery = "UPDATE task ";
        foreach($task as $name => $value) {
            if(!is_null($value)) {
                // don't forget space between value
                $sqlQuery .= " ".$task::convertNameVarToDB($name)."='".$value."',";
            }
        }
        $sqlQuery= substr($sqlQuery,0,-1)." Where id='".$task->getId()."' ;";
        var_dump($sqlQuery);
        $mc->exec($sqlQuery);
    }

    public function insertTaskToDatabase(PDO $mc, TaskItem $task) {
        $sqlQuery = "INSERT INTO task (";
        $values =") VALUES (";
        foreach( $task as $name => $value ){
            if(!empty($value)){
                $sqlQuery .= $task::convertNameVarToDB($name).",";
                $values .= "'".$value."',";
            }

        }
        $sqlQuery = substr($sqlQuery,0,-1) . substr($values,0,-1).");";
        $mc->exec($sqlQuery);
    }


    /**
     * @param int $id
     * @return TaskItem
     */
    public function getTaskItem(int $id) : ?TaskItem
    {
        foreach ($this->list as &$item) {
            if($item->getId() == $id) {
                return $item;
            }
        }
        return null;
    }

    public function getTasks() : array
    {
        return $this->list;
    }


    /**
     * @param TaskItem $task
     */
    public function addTaskItem(TaskItem $task) : void
    {
        $this->list[] = $task;
    }

    /**
     * @param TaskItem $task
     */
    public function deleteTask(TaskItem $task) : void
    {
        unset( $this->list[array_search($task->getId(), array_column($this->list,$task->getId()))]);
    }

}